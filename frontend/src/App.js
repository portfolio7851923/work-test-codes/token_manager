import React, { useState } from 'react';
import axios from 'axios';
import { saveAs } from 'file-saver';
import AddToken from './components/AddToken';
import TokenList from './components/TokenList';
import UpdateToken from './components/UpdateToken';
import DeleteToken from './components/DeleteToken';
import UploadCSV from './components/UploadCSV';
import './App.css';

const API_URL = process.env.REACT_APP_API_URL;

function App() {
    const [tokens, setTokens] = useState([]);
    const [csvUploaded, setCsvUploaded] = useState(false);

    const fetchTokens = () => {
        axios.get(`${API_URL}/api/tokens`)
            .then((response) => {
                setTokens(response.data);
            })
            .catch((error) => {
                alert(`トークンの取得中にエラーが発生しました: ${error.message}`);
            });
    };

    const handleSave = () => {
        const csvContent = tokens.map((token) => `${token.projectName},${token.token},${token.permission},${token.userId},${token.userName},${token.expiryDate},`).join('\n');

        const blob = new Blob([csvContent], {
            type: 'text/csv;charset=utf-8;',
        });
        const fileName = window.prompt('保存するファイル名を入力してください:', 'tokens.csv');
        if (fileName) {
            saveAs(blob, fileName);
        }
    };

    const handleUploadComplete = () => {
        fetchTokens();
        setCsvUploaded(true);
    };

    return (
        <div className="container">
            <h1>トークン管理</h1>
            <UploadCSV onUploadComplete={handleUploadComplete} />
            <AddToken onTokenAdded={fetchTokens} />
            <UpdateToken onTokenUpdated={fetchTokens} />
            <DeleteToken onTokenDeleted={fetchTokens} />
            {csvUploaded && <TokenList tokens={tokens} />}
            <button className="save-button" onClick={handleSave}>
                CSVとして保存
            </button>
        </div>
    );
}

export default App;
