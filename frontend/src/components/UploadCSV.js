import React, { useState } from 'react';
import axios from 'axios';

const API_URL = process.env.REACT_APP_API_URL;

const UploadCSV = ({ onUploadComplete }) => {
    const [file, setFile] = useState(null);

    const handleFileChange = (e) => {
        setFile(e.target.files[0]);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('file', file);

        axios
            .post(`${API_URL}/api/upload`, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            })
            .then((response) => {
                console.log(
                    'ファイルが正常にアップロードされました:',
                    response.data,
                );
                onUploadComplete(); // アップロード完了後に呼び出す
            })
            .catch((error) => {
                console.error(
                    'ファイルのアップロード中にエラーが発生しました!',
                    error,
                );
            });
    };

    return (
        <form onSubmit={handleSubmit}>
            <h2>CSVをアップロード</h2>
            <input type="file" onChange={handleFileChange} />
            <button type="submit">アップロード</button>
        </form>
    );
};

export default UploadCSV;
