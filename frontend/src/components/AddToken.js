import React, { useState } from 'react';
import axios from 'axios';

const API_URL = process.env.REACT_APP_API_URL;

const AddToken = ({ onTokenAdded }) => {
    const [formData, setFormData] = useState({
        projectName: '',
        token: '',
        permission: '',
        userId: '',
        userName: '',
        expiryDate: '',
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value,
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const newToken = {
            projectName: formData.projectName,
            token: formData.token,
            permission: formData.permission,
            userId: formData.userId,
            userName: formData.userName,
            expiryDate: formData.expiryDate,
        };

        axios
            .post(`${API_URL}/api/tokens`, newToken)
            .then((response) => {
                console.log('トークンが追加されました:', response.data);
                onTokenAdded(); // トークン追加後に呼び出す
            })
            .catch((error) => {
                console.error('トークンの追加中にエラーが発生しました!', error);
            });
    };

    return (
        <form onSubmit={handleSubmit}>
            <h2>トークンを追加</h2>
            <input
                type="text"
                name="projectName"
                placeholder="プロジェクト名"
                value={formData.projectName}
                onChange={handleChange}
            />
            <input
                type="text"
                name="token"
                placeholder="トークン"
                value={formData.token}
                onChange={handleChange}
            />
            <input
                type="text"
                name="permission"
                placeholder="権限"
                value={formData.permission}
                onChange={handleChange}
            />
            <input
                type="text"
                name="userId"
                placeholder="ユーザーID"
                value={formData.userId}
                onChange={handleChange}
            />
            <input
                type="text"
                name="userName"
                placeholder="ユーザー名"
                value={formData.userName}
                onChange={handleChange}
            />
            <input
                type="text"
                name="expiryDate"
                placeholder="有効期限"
                value={formData.expiryDate}
                onChange={handleChange}
            />
            <button type="submit">トークンを追加</button>
        </form>
    );
};

export default AddToken;
