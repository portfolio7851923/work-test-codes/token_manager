import React, { useState } from 'react';
import axios from 'axios';

const API_URL = process.env.REACT_APP_API_URL;

const UpdateToken = ({ onTokenUpdated }) => {
    const [projectName, setProjectName] = useState('');
    const [tokens, setTokens] = useState([]);

    const handleProjectNameChange = (e) => {
        setProjectName(e.target.value);
    };

    const handleTokenChange = (index, e) => {
        const newTokens = [...tokens];
        newTokens[index][e.target.name] = e.target.value;
        setTokens(newTokens);
    };

    const handleFetchTokens = () => {
        axios
            .get(`${API_URL}/api/tokens?projectName=${projectName}`)
            .then((response) => {
                setTokens(response.data);
            })
            .catch((error) => {
                console.error('トークンの取得中にエラーが発生しました!', error);
            });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        axios
            .put(`${API_URL}/api/tokens`, { projectName, tokens })
            .then((response) => {
                console.log('トークンが更新されました:', response.data);
                onTokenUpdated(); // トークン更新後に呼び出す
            })
            .catch((error) => {
                console.error('トークンの更新中にエラーが発生しました!', error);
            });
    };

    return (
        <div>
            <h2>トークンを更新</h2>
            <input
                type="text"
                placeholder="プロジェクト名"
                value={projectName}
                onChange={handleProjectNameChange}
            />
            <button onClick={handleFetchTokens}>トークンを取得</button>
            <form onSubmit={handleSubmit}>
                {tokens && tokens.map((token, index) => (
                    <div key={index}>
                        <input
                            type="text"
                            name="token"
                            placeholder="トークン"
                            value={token.token}
                            onChange={(e) => handleTokenChange(index, e)}
                        />
                        <input
                            type="text"
                            name="permission"
                            placeholder="権限"
                            value={token.permission}
                            onChange={(e) => handleTokenChange(index, e)}
                        />
                        <input
                            type="text"
                            name="userId"
                            placeholder="ユーザーID"
                            value={token.userId}
                            onChange={(e) => handleTokenChange(index, e)}
                        />
                        <input
                            type="text"
                            name="userName"
                            placeholder="ユーザー名"
                            value={token.userName}
                            onChange={(e) => handleTokenChange(index, e)}
                        />
                        <input
                            type="text"
                            name="expiryDate"
                            placeholder="有効期限"
                            value={token.expiryDate}
                            onChange={(e) => handleTokenChange(index, e)}
                        />
                    </div>
                ))}
                <button type="submit">トークンを更新</button>
            </form>
        </div>
    );
};

export default UpdateToken;
