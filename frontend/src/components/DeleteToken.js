import React, { useState } from 'react';
import axios from 'axios';

const API_URL = process.env.REACT_APP_API_URL;

const DeleteToken = ({ onTokenDeleted }) => {
    const [lineNum, setLineNum] = useState('');

    const handleChange = (e) => {
        setLineNum(e.target.value);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const indexToDelete = parseInt(lineNum, 10) - 1; // 表示される番号からインデックスを計算

        if (
            indexToDelete >= 0 &&
            window.confirm('本当にこのトークンを削除しますか?')
        ) {
            axios
                .delete(
                    `${API_URL}/api/tokens?line=${indexToDelete}`,
                )
                .then((response) => {
                    console.log('トークンが削除されました:', response.data);
                    onTokenDeleted(); // トークン削除後に呼び出す
                })
                .catch((error) => {
                    console.error(
                        'トークンの削除中にエラーが発生しました!',
                        error,
                    );
                });
        } else {
            alert('無効な行番号です');
        }
    };

    return (
        <form onSubmit={handleSubmit}>
            <h2>トークンを削除</h2>
            <input
                type="text"
                name="lineNum"
                placeholder="行番号"
                value={lineNum}
                onChange={handleChange}
            />
            <button type="submit">トークンを削除</button>
        </form>
    );
};

export default DeleteToken;
