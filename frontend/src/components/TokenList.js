import React from 'react';

const TokenList = ({ tokens }) => {
    return (
        <div>
            <h2>Token List</h2>
            <ul>
                {tokens.map((token, index) => (
                    <li key={index}>
                        {index + 1}. {token.projectName} - {token.token} -{' '}
                        {token.permission} - {token.userId} - {token.userName} -{' '}
                        {token.expiryDate}
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default TokenList;
